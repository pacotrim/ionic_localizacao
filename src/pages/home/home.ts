import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public lon;
  public lat;

  constructor(public navCtrl: NavController, public geolocation: Geolocation) {
  }

  ionViewDidLoad(){
    this.getLocation();
  }

  getLocation(){
    this.geolocation.getCurrentPosition().then((res) => {
      this.lat=res.coords.latitude;
      this.lon=res.coords.longitude
  }
    );
}
}